/* Simplistic glut stub for kos

   Only supports 1 window in fullscreen mode, regardless of what is called.
   Added for improved ease of portability.

   Copyright (C) 2022 T_chan
*/
#ifndef __GL_GLUT_H
#define __GL_GLUT_H

#include <GL/gl.h>

#ifdef __cplusplus
extern "C" {
#endif

/* display mode bit masks */
#define GLUT_RGB                        0x0000
#define GLUT_RGBA                       GLUT_RGB
#define GLUT_INDEX                      0x0001
#define GLUT_SINGLE                     0x0000
#define GLUT_DOUBLE                     0x0002
#define GLUT_ACCUM                      0x0004
#define GLUT_ALPHA                      0x0008
#define GLUT_DEPTH                      0x0010
#define GLUT_STENCIL                    0x0020
#define GLUT_MULTISAMPLE                0x0080
#define GLUT_STEREO                     0x0100
#define GLUT_LUMINANCE                  0x0200

/* directional keys */
#define GLUT_KEY_LEFT                   0x0064
#define GLUT_KEY_UP                     0x0065
#define GLUT_KEY_RIGHT                  0x0066
#define GLUT_KEY_DOWN                   0x0067

/* Initialization */
GLAPI void APIENTRY glutInit(int *argcp, char **argv);
GLAPI void APIENTRY glutInitWindowPosition(int x, int y);
GLAPI void APIENTRY glutInitWindowSize(int width, int height);
GLAPI void APIENTRY glutInitDisplayMode(unsigned int mode);

/* Beginning Event Processing */
GLAPI void APIENTRY glutMainLoop(void);

/* Window Management */
GLAPI int APIENTRY glutCreateWindow(char *name);
GLAPI void APIENTRY glutDestroyWindow(int win);
GLAPI void APIENTRY glutSwapBuffers();
GLAPI void APIENTRY glutFullScreen(void);

/* Callback Registration */
GLAPI void APIENTRY glutDisplayFunc(void (*func)(void));
GLAPI void APIENTRY glutIdleFunc(void (*func)(void));
GLAPI void APIENTRY glutKeyboardFunc(void (*func)(unsigned char key, int x, int y));
GLAPI void APIENTRY glutReshapeFunc(void (*func)(int width, int height));
GLAPI void APIENTRY glutSpecialFunc(void (*func)(int key, int x, int y));

extern GLboolean EXIT_GLUT_LOOP;
extern GLboolean RESHAPE_NEEDED;
extern int glutWidth;
extern int glutHeight;

typedef void (*DisplayFunc)(void);
typedef void (*ReshapeFunc)(int width, int height);
typedef void (*KeyboardFunc)(unsigned char key, int x, int y);
typedef void (*SpecialFunc)(int key, int x, int y);

extern DisplayFunc glutDisplay;
extern DisplayFunc glutIdle;
extern ReshapeFunc glutReshape;
extern KeyboardFunc glutKeyboard;
extern SpecialFunc glutSpecial;

#define _GLUT_UNUSED(x) (void)(x)

#ifdef __cplusplus
}
#endif

#endif