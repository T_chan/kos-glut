/* Simplistic glut stub for kos

   Only supports 1 window in fullscreen mode, regardless of what is called.
   Added for improved ease of portability/samples, not really to recommend using this 
   for serious things... unless somebody wants to seriously improve this...

   Copyright (C) 2022 T_chan
*/
#include <kos.h>
#include <GL/glkos.h>

#include "../include/GL/glut.h"


GLboolean EXIT_GLUT_LOOP = GL_FALSE;
GLboolean RESHAPE_NEEDED = GL_TRUE;
int glutWidth, glutHeight;
maple_device_t* cont = NULL;
cont_state_t prevstate;

DisplayFunc glutDisplay = (DisplayFunc)NULL;
DisplayFunc glutIdle = (DisplayFunc)NULL;
ReshapeFunc glutReshape = (ReshapeFunc)NULL;
KeyboardFunc glutKeyboard = (KeyboardFunc)NULL;
SpecialFunc glutSpecial = (SpecialFunc)NULL;

/* initialize the GLUT library */
GLAPI void APIENTRY glutInit(int *argcp, char **argv) {
    _GLUT_UNUSED(argcp);
    _GLUT_UNUSED(argv);
    
    glKosInit();
    glutWidth = 640;
    glutHeight = 480;
}

/* set the initial window position */
GLAPI void APIENTRY glutInitWindowPosition(int x, int y) {
    _GLUT_UNUSED(x);
    _GLUT_UNUSED(y);
}

/* set the initial window size */
GLAPI void APIENTRY glutInitWindowSize(int width, int height) {
    glutWidth  = width;
    glutHeight = height;
}

/* set the initial display mode */
GLAPI void APIENTRY glutInitDisplayMode(unsigned int mode) {
    _GLUT_UNUSED(mode);
}

/* read the Dreamcast controller, and convert to keyboard keys.
   Not really a match, since this currently doesn't work if the button is not released...
   to be improved...
 */
void readDCController(void) {
    if (!cont) return;
    cont_state_t* state = (cont_state_t *)maple_dev_status(cont);
    if (!state) return;

    if (glutKeyboard) {
        if ((prevstate.buttons & CONT_START) &&
            !(state->buttons & CONT_START)) {
                glutKeyboard(27, 0, 0);/* mapped to ESCAPE */
        }
        if ((prevstate.buttons & CONT_A) &&
            !(state->buttons & CONT_A)) {
                glutKeyboard(97, 0, 0);/* mapped to 'a' */
        }
        if ((prevstate.buttons & CONT_B) &&
            !(state->buttons & CONT_B)) {
                glutKeyboard(98, 0, 0);/* mapped to 'b' */
        }
        if ((prevstate.buttons & CONT_X) &&
            !(state->buttons & CONT_X)) {
                glutKeyboard(120, 0, 0);/* mapped to 'x' */
        }
        if ((prevstate.buttons & CONT_Y) &&
            !(state->buttons & CONT_Y)) {
                glutKeyboard(121, 0, 0);/* mapped to 'y' */
        }
    }

    if (glutSpecial) {
        if ((prevstate.buttons & CONT_DPAD_LEFT) &&
            !(state->buttons & CONT_DPAD_LEFT)) {
                glutSpecial(GLUT_KEY_LEFT, 0, 0);
        }
        if ((prevstate.buttons & CONT_DPAD_UP) &&
            !(state->buttons & CONT_DPAD_UP)) {
                glutSpecial(GLUT_KEY_UP, 0, 0);
        }
        if ((prevstate.buttons & CONT_DPAD_RIGHT) &&
            !(state->buttons & CONT_DPAD_RIGHT)) {
                glutSpecial(GLUT_KEY_RIGHT, 0, 0);
        }
        if ((prevstate.buttons & CONT_DPAD_DOWN) &&
            !(state->buttons & CONT_DPAD_DOWN)) {
                glutSpecial(GLUT_KEY_DOWN, 0, 0);
        }
    }

    memcpy ((void*)&prevstate, (void*)state, sizeof(cont_state_t));
}

/* enter the GLUT event processing loop */
GLAPI void APIENTRY glutMainLoop(void) {

    while (!EXIT_GLUT_LOOP) {
        if (RESHAPE_NEEDED && glutReshape) {
            glutReshape(glutWidth, glutHeight);
            RESHAPE_NEEDED = GL_FALSE;
        }

        readDCController();
        
        if (glutDisplay) glutDisplay();
        //TODO: call glutIdle
    }
}

/* create a top-level window */
GLAPI int APIENTRY glutCreateWindow(char *name) {
    EXIT_GLUT_LOOP = GL_FALSE;
    return 1;
}

/* destroy the specified window */
GLAPI void APIENTRY glutDestroyWindow(int win) {
    _GLUT_UNUSED(win);
    EXIT_GLUT_LOOP = GL_TRUE;
}

/* swaps the buffers of the current window if double buffered */
GLAPI void APIENTRY glutSwapBuffers() {
    glKosSwapBuffers();
}

/* requests that the current window be made full screen */
GLAPI void APIENTRY glutFullScreen(void) {
    RESHAPE_NEEDED = GL_TRUE;
}

/* set the display callback for the current window */
GLAPI void APIENTRY glutDisplayFunc(void (*func)(void)) {
    glutDisplay = (DisplayFunc)func;
}

/* set the global idle callback */
GLAPI void APIENTRY glutIdleFunc(void (*func)(void)) {
    glutIdle = (DisplayFunc)func;
}

/* set the reshape callback for the current window */
GLAPI void APIENTRY glutReshapeFunc(void (*func)(int width, int height)) {
    glutReshape = (ReshapeFunc)func;
    RESHAPE_NEEDED = GL_TRUE;
}

/* set the keyboard callback for the current window */
GLAPI void APIENTRY glutKeyboardFunc(void (*func)(unsigned char key, int x, int y)) {
    glutKeyboard = (KeyboardFunc)func;
    if (func && !cont){
        cont = maple_enum_type(0, MAPLE_FUNC_CONTROLLER);
    }
}

/* set the special keyboard callback for the current window */
GLAPI void APIENTRY glutSpecialFunc(void (*func)(int key, int x, int y)) {
    glutSpecial = (SpecialFunc)func;
    if (func && !cont){
        cont = maple_enum_type(0, MAPLE_FUNC_CONTROLLER);
    }
}
